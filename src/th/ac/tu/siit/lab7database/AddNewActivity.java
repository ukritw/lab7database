package th.ac.tu.siit.lab7database;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

//Class manages database structure. manages the table
public class AddNewActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new);
		
		Intent data = this.getIntent();
		EditText etName = (EditText)findViewById(R.id.etName);	
		etName.setText(data.getStringExtra("name"));
		
		EditText etPhone = (EditText)findViewById(R.id.etPhone);	
		etPhone.setText(data.getStringExtra("phone"));
		
		RadioButton rdHome = (RadioButton)findViewById(R.id.rdHome);
		RadioButton rdMobile = (RadioButton)findViewById(R.id.rdMobile);
		EditText etEmail = (EditText)findViewById(R.id.etEmail);
		
		int f3 = Integer.parseInt(data.getStringExtra("type"));
			//etPhone.setText(f3+","+R.drawable.home);
			if (f3==R.drawable.home) {
				rdHome.setChecked(true);
			}
			else if (f3==R.drawable.mobile) {
				rdMobile.setChecked(true);
			}
			try{
				String f4 = data.getStringExtra("email");	
				etEmail.setText(f4);
			}	catch (NullPointerException e) {}
					
				
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_new, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_save:
			EditText etName = (EditText)findViewById(R.id.etName);
			EditText etPhone = (EditText)findViewById(R.id.etPhone);
			EditText etEmail = (EditText)findViewById(R.id.etEmail);
			RadioGroup rdg = (RadioGroup)findViewById(R.id.rdgType);
			
			String sName = etName.getText().toString().trim();
			String sPhone = etPhone.getText().toString().trim();
			String sEmail = etEmail.getText().toString().trim();
			int iType = rdg.getCheckedRadioButtonId();
			
			if (sName.length() == 0 || sPhone.length() == 0 || iType == -1) {
				Toast t = Toast.makeText(this, 
						"Contact name,  phone, and type are required", 
						Toast.LENGTH_LONG);
				t.show();
			}
			else {
				Intent data = new Intent();
				data.putExtra("name", sName);
				data.putExtra("phone", sPhone);
				data.putExtra("email", sEmail);
				switch(iType) {
				case R.id.rdHome:
					data.putExtra("type", String.valueOf(R.drawable.home));
					break;
				case R.id.rdMobile:
					data.putExtra("type", String.valueOf(R.drawable.mobile));
					break;
				}
				setResult(RESULT_OK, data);
				finish();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
}
